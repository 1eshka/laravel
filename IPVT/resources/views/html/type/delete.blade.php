<h1>Удаление типа</h1>

{!! Form::model(
    $type, [
        'route' =>  [
            'type.destroy',
            'id' => $type->id
        ]
    ])!!}
{!! Form::submit('Удалить')!!}
{!! Form::close() !!}

