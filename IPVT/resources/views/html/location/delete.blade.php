<h1>Удаление места трансляции</h1>

{!! Form::model(
    $location, [
        'route' =>  [
            'location.destroy',
            'id' => $location->id
        ]
    ])!!}
{!! Form::submit('Удалить')!!}
{!! Form::close() !!}

