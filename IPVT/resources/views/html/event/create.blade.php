<h1>Добавление события</h1>

{!! Form::model(
    $event, [
        'route' =>  [
            'event.store'
        ]
    ])!!}
{!! Form::label('title','Название'); !!}
{!! Form::text('title')!!}


{!! Form::label('description','Описание'); !!}
{!! Form::text('description')!!}

{!! Form::label('data','Дата'); !!}
{!! Form::text('data')!!}

{!! Form::label('responsible','Ответственный'); !!}
{!! Form::text('responsible')!!}

{!! Form::label('URL_video','Адрес видеоплеера'); !!}
{!! Form::text('URL_video')!!}

{!! Form::label('location','Место трансляции'); !!}
{!! Form::select('location_id', $locations); !!}

{!! Form::label('type','Раздел трансляции'); !!}
{!! Form::select('type_id', $types); !!}

{!! Form::submit('Сохранить')!!}
{!! Form::close() !!}

