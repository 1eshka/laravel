<h1>Удаление места трансляции</h1>

{!! Form::model(
    $event, [
        'route' =>  [
            'event.destroy',
            'id' => $event->id
        ]
    ])!!}
{!! Form::submit('Удалить')!!}
{!! Form::close() !!}

