<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\location;

class LocationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('html.location.index', [
            'location' => location::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('html.location.create',[
            'location' => new location()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->only(['title','address_and_description']);
        //print_r($input);
        $location = new location();
        $location->title = $input['title'];
        $location->address_and_description = $input['address_and_description'];
        $location->save();
        return redirect(route('location.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $location = location::find($id);
        return view('html.location.edit',[
            'location' => $location
        ]);
    }

    public function delete($id)
    {
        $location = location::find($id);
        //find вытащит из БД категорию с первичным ключом равным id
        return view ('html.location.delete',[
            'location'=>$location
        ]);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $location = location::find($id);
        //
        $input = $request->only(['title','address_and_description']);
        $location->title = $input['title'];
        $location->address_and_description = $input['address_and_description'];
        $location->update();
        return redirect(route('location.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $location = location::find($id);
        $location -> delete();
        return redirect(route('location.index'));
    }
}
