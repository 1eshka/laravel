<?php

namespace App\Http\Controllers;

use App\Category;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Возвращаем обработанный шаблон
        // resources/views/html/category/index.blade.php
        return view('html.category.index', [
            'categories' => Category::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('html.category.create',[
            'category' => new Category()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
     {
        //должна быть валидация из лекции 22.10.15
        $input = $request->only(['title']);
        //print_r($input);
        $category = new Category();
        $category->title = $input['title'];
        $category->save();
        return redirect(route('category.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Из БД вытащим категорию с указанным ID
        $category = Category::find($id);
        return view('html.category.edit',[
            'category' => $category
        ]);
    }
    public function delete($id)
    {
        $category = Category::find($id);
        //find вытащит из БД категорию с первичным ключом равным id
        return view ('html.category.delete',['category'=>$category ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->only(['id', 'title']);
        // Из БД вытащим категорию с указанным ID
        $category = Category::find($input['id']);
        $category->title = $input['title'];
        $category->save();
        return redirect(route('category.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $input = $request->only(['id']);
        $category = Category::find($input['id']);
        $category->delete();
        //print_r($input);
        return redirect(route('category.index'));
    }
}
