<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Type;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('html.type.index', [
            'types' => Type::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('html.type.create',[
            'type' => new Type()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //должна быть валидация из лекции 22.10.15
        $input = $request->only(['title','description']);
        //print_r($input);
        $type = new Type();
        $type->title = $input['title'];
        $type->description = $input['description'];
        $type->save();
        return redirect(route('type.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $type = Type::find($id);
        return view('html.type.edit',[
            'type' => $type
        ]);
    }

    
    public function delete($id)
    {
        $type = Type::find($id);
        //find вытащит из БД категорию с первичным ключом равным id
        return view ('html.type.delete',[
            'type'=>$type
        ]);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = Type::find($id);
        //
        $input = $request->only(['title','description']);
        $type->title = $input['title'];
        $type->description = $input['description'];
        $type->update();
        return redirect(route('type.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = Type::find($id);
        $type -> delete();
        return redirect(route('type.index'));
    }
}
