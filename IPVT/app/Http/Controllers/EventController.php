<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\event;
use App\Type;
use App\location;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('html.event.index', [
            'event' => event::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('html.event.create',[
           'event' => new Event(),
           'types' => Type::lists('title', 'id'),
           'locations' => location::lists('title', 'id'), 
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->only(['title','description','data','responsible','URL_video','type','location']);
        //print_r($input);
        $event = new event();
        $event->title = $input['title'];
        $event->description = $input['description'];
        $event->data = $input['data'];
        $event->responsible = $input['responsible'];
        $event->URL_video = $input['URL_video'];
        $event->type = $input['types'];
        $event->location = $input['locations'];
        $event->save();
        return redirect(route('event.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $event = event::find($id);
        return view('html.event.edit',[
            'event' => event
        ]);
    }

    public function delete($id)
    {
        $event = event::find($id);
        //find вытащит из БД категорию с первичным ключом равным id
        return view ('html.event.delete',[
            'event'=>$event
        ]);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $event = event::find($id);
        //
        $input = $request->only(['title','address_and_description']);
        $event->title = $input['title'];
        $event->description = $input['description'];
        $event->data = $input['data'];
        $event->responsible = $input['responsible'];
        $event->URL_video = $input['URL_video'];
        $event->type = $input['type'];
        $event->location = $input['location'];
        $event->update();
        return redirect(route('location.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $location = location::find($id);
        $location -> delete();
        return redirect(route('location.index'));
    }
}
