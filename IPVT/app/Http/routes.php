<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('home', [
    'uses' => 'TypeController@create', // Какой контроллер вызывается
    'as'   => 'type.create' // Именованный маршрут
]);

//Route::get('user/{name?}', 'UserController@index');


//---------------------------------

//Вывод формы и создание новго типа
Route::get('type/create', [
    'uses' => 'TypeController@create', // Какой контроллер вызывается
    'as'   => 'type.create' // Именованный маршрут
]);

//Обработка формы и Сохранение новой записи в БД
Route::post('type/store', [
    'uses' => 'TypeController@store', // Какой контроллер вызывается
    'as'   => 'type.store' // Именованный маршрут
]);

//---------------------------------

//Вывод списка всех типов
Route::get('type/index', [
    'uses' => 'TypeController@index', // Какой контроллер вызывается
    'as'   => 'type.index' // Именованный маршрут
]);

//---------------------------------

//Вывод формы редактирования товара с указанным id
Route::get('type/edit/{id}', [
    'uses' => 'TypeController@edit', // Какой контроллер вызывается
    'as'   => 'type.edit' // Именованный маршрут
]);

Route::post('type/update/{id}', [
    'uses' => 'TypeController@update', // Какой контроллер вызывается
    'as'   => 'type.update' // Именованный маршрут
]);

//---------------------------------

//Вывод формы редактирования товара с указанным id
Route::get('type/delete/{id}', [
    'uses' => 'TypeController@delete', // Какой контроллер вызывается
    'as'   => 'type.delete' // Именованный маршрут
]);

Route::post('type/destroy/{id}', [
    'uses' => 'TypeController@destroy', // Какой контроллер вызывается
    'as'   => 'type.destroy' // Именованный маршрут
]);



//---------------------------------
//---------------------------------
//---------------------------------



//Вывод формы и создание новго типа
Route::get('location/create', [
    'uses' => 'LocationsController@create', // Какой контроллер вызывается
    'as'   => 'location.create' // Именованный маршрут
]);

//Обработка формы и Сохранение новой записи в БД
Route::post('location/store', [
    'uses' => 'LocationsController@store', // Какой контроллер вызывается
    'as'   => 'location.store' // Именованный маршрут
]);

//---------------------------------

//Вывод списка всех типов
Route::get('location/index', [
    'uses' => 'LocationsController@index', // Какой контроллер вызывается
    'as'   => 'location.index' // Именованный маршрут
]);

//---------------------------------

//Вывод формы редактирования товара с указанным id
Route::get('location/edit/{id}', [
    'uses' => 'LocationsController@edit', // Какой контроллер вызывается
    'as'   => 'location.edit' // Именованный маршрут
]);

Route::post('location/update/{id}', [
    'uses' => 'LocationsController@update', // Какой контроллер вызывается
    'as'   => 'location.update' // Именованный маршрут
]);

//---------------------------------

//Вывод формы редактирования товара с указанным id
Route::get('location/delete/{id}', [
    'uses' => 'LocationsController@delete', // Какой контроллер вызывается
    'as'   => 'location.delete' // Именованный маршрут
]);

Route::post('location/destroy/{id}', [
    'uses' => 'LocationsController@destroy', // Какой контроллер вызывается
    'as'   => 'location.destroy' // Именованный маршрут
]);


//---------------------------------
//---------------------------------
//---------------------------------



//Вывод формы и создание новго типа
Route::get('event/create', [
    'uses' => 'EventController@create', // Какой контроллер вызывается
    'as'   => 'event.create' // Именованный маршрут
]);

//Обработка формы и Сохранение новой записи в БД
Route::post('event/store', [
    'uses' => 'EventController@store', // Какой контроллер вызывается
    'as'   => 'event.store' // Именованный маршрут
]);

//---------------------------------

//Вывод списка всех типов
Route::get('event/index', [
    'uses' => 'EventController@index', // Какой контроллер вызывается
    'as'   => 'event.index' // Именованный маршрут
]);

//---------------------------------

//Вывод формы редактирования товара с указанным id
Route::get('event/edit/{id}', [
    'uses' => 'EventController@edit', // Какой контроллер вызывается
    'as'   => 'event.edit' // Именованный маршрут
]);

Route::post('event/update/{id}', [
    'uses' => 'EventController@update', // Какой контроллер вызывается
    'as'   => 'event.update' // Именованный маршрут
]);

//---------------------------------

//Вывод формы редактирования товара с указанным id
Route::get('event/delete/{id}', [
    'uses' => 'EventController@delete', // Какой контроллер вызывается
    'as'   => 'event.delete' // Именованный маршрут
]);

Route::post('event/destroy/{id}', [
    'uses' => 'EventController@destroy', // Какой контроллер вызывается
    'as'   => 'event.destroy' // Именованный маршрут
]);