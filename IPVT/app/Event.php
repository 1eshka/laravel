<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    public function types()
    {
        return $this->belongsTo('App\Type');
    }
}
