<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class HelloController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string                    $username
     * @return \Illuminate\Http\Response
     */
    public function hello(Request $request, $username)
    {
        return view('hello', ['username' => $username]);
    }
}
