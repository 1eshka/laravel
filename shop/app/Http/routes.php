<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/categories/index/', [
    'uses' => 'CategoryController@index', // Что будет вызвано
    'as'   => 'category.index' // Создаём именованный маршрут
]);

// Форма добавления категории
Route::get('/categories/create/', [
    'uses' => 'CategoryController@create', // Что будет вызвано
    'as'   => 'category.create' // Создаём именованный маршрут
]);

// Обработчик формы добавления категории
Route::post('/categories/store/', [
    'uses' => 'CategoryController@store', // Что будет вызвано
    'as'   => 'category.store' // Создаём именованный маршрут
]);

Route::get('/product/create', 'ProductController@create');

Route::post('/product/store', [
    'uses' => 'ProductController@store', 
	'as' => 'product.store'
]);













